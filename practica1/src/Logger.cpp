#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <unistd.h>

#include <ctype.h>
#include <stdlib.h>


#define MAX_BUF 200
#define fifo "/tmp/fifo_log"

int main()
{
	int fd;
	char buffer[MAX_BUF];

	mkfifo(fifo,0666);

	fd=open(fifo,O_RDONLY);

	if (fd == -1){
		perror("Error al abrir el fifo\n");
		exit(1);
	}
	
	printf("Logger preparado para leer.\n");
	printf("Comienza la partida:\n");	

	while(1)
	{
		read(fd,buffer,MAX_BUF);
		if (buffer[0]=='D')
			break;
		printf("%s\n",buffer);
	}

	close(fd);
	unlink(fifo);
}
