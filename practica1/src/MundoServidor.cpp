// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

//Includes for logger
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <sys/file.h>

#define fifo "/tmp/fifo_log"
#define fifo2Client "/tmp/fifo_data"
#define fifoFromClient "/tmp/fifo_keys"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundoServidor::CMundoServidor()
{
	Init();
}

CMundoServidor::~CMundoServidor()
{
	//Para logger
	char buffer[50];
	sprintf(buffer,"Destuctor mundo");
	write(fd,buffer,strlen(buffer)+1);
	close(fd);

	//Cierra el FIFO de datos
	char buffer2[50];
	sprintf(buffer2,"Servidor desconectado");
	write(fd_data2Client,buffer,strlen(buffer)+1);
	close(fd_data2Client);

	//Cierra el FIFO de teclas
	close(fd_keyFromClient);
}

void* hilo_comandos (void* d){

	CMundoServidor* p = (CMundoServidor*) d;
	p->RecibeComandosJugador();

}


void CMundoServidor::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoServidor::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoServidor::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	bool choque1 = jugador1.Rebota(esfera);
	bool choque2 = jugador2.Rebota(esfera);

	// Por cada rebote, la pelota disminuye un 10% su radio
	// y aumenta un 10% su velocidad.
	if (choque1 || choque2)
	{
		esfera.radio = esfera.radio-esfera.radio * 0.1;
		esfera.velocidad.x = esfera.velocidad.x + esfera.velocidad.x * 0.1;
		esfera.velocidad.y = esfera.velocidad.y + esfera.velocidad.y * 0.1;
	}

	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		esfera.radio = esfera.radio_original;
		puntos2++;

		char buffer[50];
		if(puntos2 < 3){
			sprintf(buffer,"Punto para jugador 2 - Total de %d puntos",puntos2);
			write(fd,buffer,strlen(buffer)+1);
		}
		if(puntos2 == 3){
			sprintf(buffer,"Punto para jugador 2 - Total de %d puntos\nEl jugador 2 ha ganado",puntos2);
			write(fd,buffer,strlen(buffer)+1);
			finPartida = 1;
		}
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		esfera.radio = esfera.radio_original;
		puntos1++;

		char buffer[100];
		if(puntos1 < 3){
			sprintf(buffer,"Punto para jugador 1 - Total de %d puntos",puntos1);
			write(fd,buffer,strlen(buffer)+1);
		}
		if(puntos1 == 3){
			sprintf(buffer,"Punto para jugador 1 - Total de %d puntos\nEl jugador 1 ha ganado",puntos1);
			write(fd,buffer,strlen(buffer)+1);
			finPartida = 1;
		}
	}

	//Comprobacion fin partida
	if (finPartida == 1){
		esfera.centro.x=0;
		esfera.velocidad.x=0;
		esfera.velocidad.y=0;
	}

	//Envio de datos al cliente
	char buffer[200];
	sprintf(buffer,"%f %f %f %f %f %f %f %f %f %f %f %d %d",esfera.centro.x, esfera.centro.y, esfera.radio, 
								jugador1.x1, jugador1.x2, jugador1.y1, jugador1.y2, 
								jugador2.x1, jugador2.x2, jugador2.y1, jugador2.y2,
								puntos1, puntos2);
	write(fd_data2Client,buffer,strlen(buffer)+1);

}

void CMundoServidor::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
	case 'a':jugador1.velocidad.x=-1;break;
	case 'z':jugador1.velocidad.x=1;break;
	case 'u':jugador2.velocidad.x=-1;break;
	case 'i':jugador2.velocidad.x=1;break;
	case 'w':jugador1.velocidad.y=-4;	jugador1.velocidad.x = 0;break;
	case 'q':jugador1.velocidad.y=4;	jugador1.velocidad.x = 0;break;
	case 'b':jugador2.velocidad.y=-4;	jugador2.velocidad.x = 0;break;
	case 'h':jugador2.velocidad.y=4;	jugador2.velocidad.x = 0;break;
	
	case 's':
	{
		jugador1.y1 = jugador1.y1 + 0.2;
		jugador1.y2 = jugador1.y2 + 0.2;
		jugador1.velocidad.y = 0;
		jugador1.velocidad.x = 0;
		break;
	}
	case 'x':
	{
		jugador1.y1 = jugador1.y1 - 0.2;
		jugador1.y2 = jugador1.y2 - 0.2;
		jugador1.velocidad.y = 0;
		jugador1.velocidad.x = 0;
		break;
	}
	case 'j':
	{
		jugador2.y1 = jugador2.y1 + 0.2;
		jugador2.y2 = jugador2.y2 + 0.2;
		jugador2.velocidad.y = 0;
		jugador2.velocidad.x = 0;
		break;
	}
	case 'n':
	{
		jugador2.y1 = jugador2.y1 - 0.2;
		jugador2.y2 = jugador2.y2 - 0.2;
		jugador2.velocidad.y = 0;
		jugador2.velocidad.x = 0;
		break;
	}

	}//End switch
}

void CMundoServidor::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	finPartida=0;

	fd=open(fifo,O_WRONLY); //Logger

	fd_data2Client=open(fifo2Client,O_WRONLY); //Fifo data2client
	fd_keyFromClient = open(fifoFromClient,O_RDONLY); //Fifo keyFromClient

	pthread_create(&thid1, NULL, hilo_comandos, this);
}

void CMundoServidor::RecibeComandosJugador(){
	
	while(1){
		
		usleep(10);
		char cad [100];
		read(fd_keyFromClient, cad, sizeof (cad));
		unsigned char key;

		sscanf (cad,"%c",&key);
		
		switch(key){

			case 'w':jugador1.velocidad.y=-4;	break;
			case 'q':jugador1.velocidad.y=4;	break;
			case 'b':jugador2.velocidad.y=-4;	break;
			case 'h':jugador2.velocidad.y=4;	break;
		}
	}
}

