#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <stdio.h>
#include <unistd.h>

#include <ctype.h>
#include <stdlib.h>

#include "Esfera.h"
#include "Raqueta.h"
#include "DatosMemoriaComp.h"


int main()
{
	int fd_dmc;
	DatosMemoriaComp* pdmc;
	char* proyec;

	fd_dmc = open("/tmp/datos.txt",O_RDWR);
	proyec=(char*)mmap(NULL,sizeof(*pdmc), PROT_WRITE|PROT_READ,MAP_SHARED,fd_dmc,0); //Proyectamos el fichero
	close(fd_dmc);
	pdmc = (DatosMemoriaComp*)proyec;

	while(1){
		float posicion_raqueta = (pdmc->raqueta1.y1 + pdmc->raqueta1.y2)/2;
		float diferencia = pdmc->esfera.centro.y - posicion_raqueta;
		if (diferencia < 0)
			pdmc->accion = -1;
		else if (diferencia > 0)
			pdmc->accion = 1;
		else
			pdmc->accion = 0;
		
		usleep(25000);
	}
	return (0);
}
