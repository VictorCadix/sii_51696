// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

//Includes for logger
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <sys/file.h>

#define fifoFromServer "/tmp/fifo_data"
#define fifo2Server "/tmp/fifo_keys"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundoCliente::CMundoCliente()
{
	Init();
}

CMundoCliente::~CMundoCliente()
{	
	//Fin memoria compartida (bot)
	munmap(proyec,sizeof(dmc));

	//Cierra el FIFO de datos
	close (fd_dataFromServer);
	unlink (fifoFromServer);
	
	//Cierra el FIFO de teclas
	close (fd_key2Server);
	unlink (fifo2Server);

}

void CMundoCliente::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoCliente::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoCliente::OnTimer(int value)
{	
	//Lectura de datos de la tuberia (datos)
	read(fd_dataFromServer,buffer,sizeof (buffer));
	sscanf(buffer,"%f %f %f %f %f %f %f %f %f %f %f %d %d",&esfera.centro.x, &esfera.centro.y, &esfera.radio, 
								&jugador1.x1, &jugador1.x2, &jugador1.y1, &jugador1.y2, 
								&jugador2.x1, &jugador2.x2, &jugador2.y1, &jugador2.y2,
								&puntos1, &puntos2);
	// A memoria compartida (bot)

	pdmc->raqueta1 = jugador1;
	pdmc->esfera = esfera;
	
	//OnKeyboard
	if	(pdmc->accion == 0)
		jugador1.velocidad.y = 0;
	
	else if (pdmc->accion == 1)
		OnKeyboardDown('q',0,0);

	else if (pdmc->accion == -1)
		OnKeyboardDown('w',0,0);

	
	
	
}

void CMundoCliente::OnKeyboardDown(unsigned char key, int x, int y)
{
	char bufKey[50];
	sprintf(bufKey,"%c",key);
	write(fd_key2Server,bufKey,strlen(bufKey)+1);
}

void CMundoCliente::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	finPartida=0;


	//Creacion del fichero (bot)
	
	fd_dmc = open("/tmp/datos.txt",O_RDWR|O_CREAT|O_TRUNC,0666);
	write(fd_dmc,&dmc,sizeof(dmc));
	proyec=(char*)mmap(NULL,sizeof(dmc), PROT_WRITE|PROT_READ,MAP_SHARED,fd_dmc,0);
	close(fd_dmc);
	pdmc = (DatosMemoriaComp*)proyec;
	pdmc->accion = 0;


	//Creacion Fifo dataFromServer

	mkfifo(fifoFromServer,0666);
	fd_dataFromServer=open(fifoFromServer,O_RDONLY);
	if (fd_dataFromServer == -1){
		perror("Error al abrir el FIFO (fifoFromServer)\n");
		exit(1);
	}


	//Creacion Fifo key2Server

	mkfifo(fifo2Server,0666);
	fd_key2Server=open(fifo2Server,O_WRONLY);
	if (fd_key2Server == -1){
		perror("Error al abrir el FIFO (fifo2Server)\n");
		exit(1);
	}

}
